package examples.thermostat;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Created by darkg on 21-May-17.
 */
public class ThermostatAgent extends Agent {
    private static final int MIN_TEMP = 18;
    private static final int MAX_TEMP = 22;


    @Override
    protected void setup() {

        // Request temperature updates from the env agent from time to time
        this.addBehaviour(new TickerBehaviour(this, 1000) {
            @Override
            protected void onTick() {
                ACLMessage statusMessage = new ACLMessage(ACLMessage.CFP);

                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("environment-sensor");
                template.addServices(sd);

                DFAgentDescription[] result;
                try {
                    result = DFService.search(myAgent, template);
                    for (DFAgentDescription desc : result) {
                        statusMessage.addReceiver(desc.getName());
                        System.out.println("Found environment sensor: " + desc.getName().getName());
                    }
                    myAgent.send(statusMessage);
                } catch (FIPAException e) {
                    e.printStackTrace();
                }
            }
        });

        // Parse temperature updates from the environment
        this.addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                MessageTemplate messageTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                ACLMessage message = myAgent.receive(messageTemplate);
                if(message != null) {
                    Integer sensedTemperature = Integer.parseInt(message.getContent());

                    ACLMessage reply = message.createReply();
                    reply.setPerformative(ACLMessage.PROPOSE);
                    if (sensedTemperature < MIN_TEMP) {
                        reply.setContent(TempMessageConstants.INCREASE_TEMP);
                        myAgent.send(reply);
                    } else if (sensedTemperature > MAX_TEMP) {
                        reply.setContent(TempMessageConstants.DECREASE_TEMP);
                        myAgent.send(reply);
                    }
                    System.out.println("Agent: [" + getName() + "]" + " got temperature " + sensedTemperature + " from " + message.getSender());
                } else {
                    block();
                }
            }
        });
    }

}
