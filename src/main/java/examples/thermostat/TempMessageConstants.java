package examples.thermostat;

/**
 * Created by darkg on 21-May-17.
 */
public class TempMessageConstants {
    public static final String INCREASE_TEMP = "increase";
    public static final String DECREASE_TEMP = "decrease";
}
