package examples.thermostat;


/**
 * Created by darkg on 21-May-17.
 */

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * This class is responsible for modeling the environment in which an agent lives. It
 * just holds the temperature value of the current world.
 * It gets colder with time, if the heater of this specific environment isn't on.
 */
public class EnvironmentAgent extends Agent {

    private Integer temperature;
    private boolean heaterOn = false;

    @Override
    protected void setup() {
        Object[] args = getArguments();
        if (args == null || args.length == 0) {
            doDelete();
        } else {
            // Register the book-selling service in the yellow pages
            temperature = Integer.parseInt((String) args[0]);
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.setName(getAID());
            ServiceDescription sd = new ServiceDescription();
            sd.setType("environment-sensor");
            sd.setName("JADE-temperature-sensor");
            dfd.addServices(sd);
            try {
                DFService.register(this, dfd);
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
            addBehaviours();
        }
    }

    private void addBehaviours() {
        // Add ticker increasing / decreasing temperature behaviour
        this.addBehaviour(new TickerBehaviour(this, 1000) {
            @Override
            protected void onTick() {
                if (heaterOn) {
                    temperature++;
                } else {
                    temperature--;
                }
                System.out.println(getName() + " - Temperature is now: " + temperature);
            }
        });

        this.addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage message = myAgent.receive(MessageTemplate.or(
                        MessageTemplate.MatchPerformative(ACLMessage.CFP),
                        MessageTemplate.MatchPerformative(ACLMessage.PROPOSE)
                ));
                if (message != null) {
                    if (message.getPerformative() == ACLMessage.CFP) {
                        // Send status back to agent
                        ACLMessage reply = message.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setContent(temperature.toString());
                        myAgent.send(reply);
                    } else if (message.getPerformative() == ACLMessage.PROPOSE) {
                        // Adjust sensor based on command from thermostat
                        String messageContent = message.getContent();
                        if (TempMessageConstants.DECREASE_TEMP.equals(messageContent) && heaterOn) {
                            heaterOn = false;
                            System.out.println(getName() + " turning heater off");
                        } else if (TempMessageConstants.INCREASE_TEMP.equals(messageContent) && !heaterOn) {
                            heaterOn = true;
                            System.out.println(getName() + " turning heater on");
                        } else {
                            System.out.println("Could not understand message or temperature didn't need a change");
                        }
                    }
                } else {
                    block();
                }
            }
        });

    }
}
